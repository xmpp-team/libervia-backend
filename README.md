# Libervia

### Copyrights

- Jérôme Poisson aka Goffi 2008-2023
- Adrien Cossa aka Souliane 2013-2016
- Additional contributions by community members

Libervia stands out as a one-of-a-kind XMPP (Jabber) client that brings you a
comprehensive, multi-purpose, and multi-platform solution. It doesn't just focus on
instant messaging but offers an array of tools and features all in one place.

## License

Libervia is a Libre software (Free as in "Freedom"), distributed under the AGPL v3+
license. For the full license, please refer to the `COPYING` file.

## Relevant URLs

Here are some useful URLs, all of which (except for the source code repository) are
powered by Libervia itself, showcasing the flexibility and versatility of the tool:

- Official Website: <https://libervia.org>
- Documentation: <https://libervia.org/documentation>
- Bug Tracker: <https://libervia.org/tickets>
- Source Code: <https://repos.goffi.org/libervia-backend/>
- News: <https://libervia.org/news> (currently the same as the blog below)
- Main Developer's Blog: <https://www.goffi.org/>

Additionally, our blogs are accessible via ActivityPub thanks to the Libervia XMPP <=>
ActivityPub gateway.

## About

Libervia is a unique XMPP client based on a daemon/frontends architecture. It supports
multi-frontends (desktop, web, console interface, CLI, etc.) and provides a plethora of
multi-purpose tools such as instant messaging, microblogging, photo albums, calendar
events, file sharing, A/V calls, and state-of-the-art end-to-end encryption.

Embedded within our DNA is a strong commitment to ethics. Our social contract provides
insight into these principles - refer to `CONTRAT_SOCIAL` or `CONTRAT_SOCIAL_en`.

This repository contains the "backend" of Libervia—the heart of the software. To use it,
you will need to install one or more frontends:

- [Libervia Web](https://repos.goffi.org/libervia-web/): Our flagship frontend, offering a
  potent, customizable social network for a broad spectrum of users—families, friends,
  groups, associations, public services, companies, and more.
- [Libervia Desktop](https://repos.goffi.org/libervia-desktop/): Integrated with your
  desktop, this frontend is under continual development. The existing
  [Kivy](https://kivy.org/)-based version includes a prototype Android version, with an
  upcoming iteration to be based on Qt.
- Libervia CLI: Bundled with Libervia Backend, our CLI is a true standout—one of the most
  powerful XMPP CLIs out there. It's the go-to tool for quick tasks or crafting automation
  scripts.
- Libervia TUI: Perfect for terminal enthusiasts or headless server operations, our TUI is
  included with Libervia Backend.


Libervia can also function as an XMPP server component or a "generic" plugin. Currently,
we offer two components:

- An advanced file sharing component supporting [HTTP File
  Upload](https://xmpp.org/extensions/xep-0363.html), [Jingle File
  Transfer](https://xmpp.org/extensions), file deletion, public link, access permission,
  quotas, etc.
- An XMPP <=> ActivityPub Gateway, enabling users to interact with ActivityPub content
  (from platforms like Mastodon, Pleroma, PeerTube, Mobilizon, etc.) as if they were XMPP
  blogs or private messages, and vice versa.

## How To Use It?

For installation instructions and general documentation, please refer to the `doc`
directory or visit <https://libervia.org/documentation/>

## Contributions

Here are the URIs you can use to publish/retrieve tickets or merge requests:

- Tickets: xmpp:pubsub.goffi.org?;node=org.salut-a-toi.tickets%3A0 (please use "core" label)
- Merge requests: xmpp:pubsub.goffi.org?;node=org.salut-a-toi.merge_requests%3A0 (please use "core" label)

Tickets and merge requests are managed by Libervia itself using XMPP. For more
information, visit <https://libervia.org/tickets>

## Contact

Join us on the XMPP MUC room
[libervia@chat.jabberfr.org](xmpp:libervia@chat.jabberfr.org?join) ([web
link](https://chat.jabberfr.org/converse.js/libervia@chat.jabberfr.org)), or contact us
directly:

- contact@salut-a-toi.invalid (replace `invalid` with `org`)
- goffi@goffi.invalid (email, replace `invalid` with `org`)

This software is dedicated to Roger Poisson.
