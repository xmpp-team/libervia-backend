[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "libervia-backend"
dynamic = ["version"]
description = "Libervia: A feature-rich XMPP client showcasing diverse frontends, uniting instant messaging, blogging, file sharing, and ActivityPub-XMPP interactions seamlessly."
readme = "README.md"
license = "AGPL-3.0-or-later"
requires-python = ">=3.11"
authors = [
    { name = "Libervia Dev Team", email = "contact@goffi.org" },
]
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Environment :: Console",
    "Framework :: Twisted",
    "Operating System :: POSIX :: Linux",
    "Topic :: Communications :: Chat",
    "Topic :: Internet :: XMPP"
]
dependencies = [
    "aiosqlite",
    "alembic",
    "babel < 3",
    "cryptography >= 41.0.1",
    "dbus-python < 1.3",
    "emoji ~= 2.8",
    "html2text < 2020.2",
    "jinja2 >= 3.1.2",
    "langid < 2",
    "lxml ~= 5.2",
    "lxml_html_clean >= 0.1.1",
    "markdown >= 3.0",
    "miniupnpc < 2.1",
    "mutagen < 2",
    "netifaces < 0.12",
    "oldmemo >= 1.0.0, < 2",
    "omemo >= 1.0.0, < 2",
    "pillow >= 6.0.0",
    "progressbar2 < 3.54",
    "pydantic ~= 2.4",
    "pygments < 3",
    "pygobject ~= 3.48",
    "pyopenssl ~= 24.1",
    "python-dateutil >= 2.8.1, < 3",
    # currently disabled due to incompatibility, OTR feature may be removed in
    # the close future
    # "python-potr < 1.1",
    "pyxdg < 0.30",
    "pyyaml < 7.0.0",
    "python-xlib ~= 0.33",
    "prompt_toolkit ~= 3.0",
    "rich ~= 13.5",
    "sat_tmp @ hg+https://repos.goffi.org/sat_tmp#egg=sat_tmp",
    "setuptools_scm",
    "shortuuid ~= 1.0.11",
    "sqlalchemy >= 1.4",
    "treq < 23.0.0",
    "twisted[tls] >= 22.10",
    "twomemo >= 1.0.0, < 2",
    "txdbus",
    "urwid >= 1.2.0, < 3",
    "urwid-satext @ hg+https://repos.goffi.org/urwid-satext#egg=urwid-satext",
    "wokkel >= 18.0.0, < 19.0.0",
    "xmlschema",
]
keywords = [
    "XMPP",
    "chat",
    "instant_messaging",
    "blogging",
    "photo albums",
    "ActivityPub",
    "file sharing",
    "events"
]

[project.optional-dependencies]
SVG = [
    "CairoSVG",
]
GUI = [
    "PyQt6"
]
TUI = [
    "term-image ~= 0.7.1"
]
remote-control = [
    "cbor2"
]
conferences = [
    "autobahn ~= 23.6"
]

all = [
    "libervia-backend[SVG]",
    "libervia-backend[GUI]",
    "libervia-backend[TUI]",
    "libervia-backend[remote-control]",
    "libervia-backend[conferences]"
]

[project.scripts]
jp = "libervia.cli.base:LiberviaCli.run"
li = "libervia.cli.base:LiberviaCli.run"
libervia-backend = "libervia.backend.core.launcher:Launcher.run"
libervia-cli = "libervia.cli.base:LiberviaCli.run"
libervia-tui = "libervia.tui.base:LiberviaTUIApp.run"
primitivus = "libervia.tui.base:LiberviaTUIApp.run"
sat = "libervia.backend.core.launcher:Launcher.run"

[project.urls]
Homepage = "https://libervia.org"
Documentation = "https://libervia.org/documentation"
Repository = "https://repos.goffi.org/libervia-backend/"
Roadmap = "https://libervia.org/roadmap"

[tool.hatch.build.targets.wheel]
packages = ["libervia", "twisted"]

[tool.hatch.envs.default]
# we need system-package for GPG
system-packages = true
features = ["all"]

[tool.hatch.envs.dev]
dependencies = [
    "ipdb",
    "pudb",
    "pytest",
    "pytest-twisted",
    "aiosmtpd"
]


[tool.hatch.version]
path = "libervia/backend/__init__.py"

[tool.hatch.build]
artifacts = [
    "*.ini",
    "*.yaml"
]

[tool.hatch.build.targets.wheel.shared-data]
"i18n/fr/LC_MESSAGES/libervia_backend.mo" = "share/locale/fr/LC_MESSAGES/libervia_backend.mo"
CHANGELOG = "share/doc/libervia-backend/CHANGELOG"
COPYING = "share/doc/libervia-backend/COPYING"
INSTALL = "share/doc/libervia-backend/INSTALL"
README = "share/doc/libervia-backend/README"
README4TRANSLATORS = "share/doc/libervia-backend/README4TRANSLATORS"
"misc/org.libervia.Libervia.service" = "share/dbus-1/services/org.libervia.Libervia.service"

[tool.hatch.metadata]
allow-direct-references = true
