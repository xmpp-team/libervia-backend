#!/usr/bin/env python3


# Libervia TUI
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libervia.backend.core.i18n import _
import urwid
import copy
from libervia.backend.core import exceptions
from urwid_satext import sat_widgets
from urwid_satext import files_management
from libervia.backend.core.log import getLogger

log = getLogger(__name__)
from libervia.tui.constants import Const as C
from libervia.tui.widget import LiberviaTUIWidget
from libervia.frontends.tools import xmlui


class LiberviaTUIEvents(object):
    """Used to manage change event of LiberviaTUI widgets"""

    def _event_callback(self, ctrl, *args, **kwargs):
        """ " Call xmlui callback and ignore any extra argument"""
        args[-1](ctrl)

    def _xmlui_on_change(self, callback):
        """Call callback with widget as only argument"""
        urwid.connect_signal(self, "change", self._event_callback, callback)


class LiberviaTUIEmptyWidget(xmlui.EmptyWidget, urwid.Text):
    def __init__(self, _xmlui_parent):
        urwid.Text.__init__(self, "")


class LiberviaTUITextWidget(xmlui.TextWidget, urwid.Text):
    def __init__(self, _xmlui_parent, value, read_only=False):
        urwid.Text.__init__(self, value)


class LiberviaTUILabelWidget(xmlui.LabelWidget, LiberviaTUITextWidget):
    def __init__(self, _xmlui_parent, value):
        super(LiberviaTUILabelWidget, self).__init__(_xmlui_parent, value + ": ")


class LiberviaTUIJidWidget(xmlui.JidWidget, LiberviaTUITextWidget):
    pass


class LiberviaTUIDividerWidget(xmlui.DividerWidget, urwid.Divider):
    def __init__(self, _xmlui_parent, style="line"):
        if style == "line":
            div_char = "─"
        elif style == "dot":
            div_char = "·"
        elif style == "dash":
            div_char = "-"
        elif style == "plain":
            div_char = "█"
        elif style == "blank":
            div_char = " "
        else:
            log.warning(_("Unknown div_char"))
            div_char = "─"

        urwid.Divider.__init__(self, div_char)


class LiberviaTUIStringWidget(
    xmlui.StringWidget, sat_widgets.AdvancedEdit, LiberviaTUIEvents
):
    def __init__(self, _xmlui_parent, value, read_only=False):
        sat_widgets.AdvancedEdit.__init__(self, edit_text=value)
        self.read_only = read_only

    def selectable(self):
        if self.read_only:
            return False
        return super(LiberviaTUIStringWidget, self).selectable()

    def _xmlui_set_value(self, value):
        self.set_edit_text(value)

    def _xmlui_get_value(self):
        return self.get_edit_text()


class LiberviaTUIJidInputWidget(xmlui.JidInputWidget, LiberviaTUIStringWidget):
    pass


class LiberviaTUIPasswordWidget(
    xmlui.PasswordWidget, sat_widgets.Password, LiberviaTUIEvents
):
    def __init__(self, _xmlui_parent, value, read_only=False):
        sat_widgets.Password.__init__(self, edit_text=value)
        self.read_only = read_only

    def selectable(self):
        if self.read_only:
            return False
        return super(LiberviaTUIPasswordWidget, self).selectable()

    def _xmlui_set_value(self, value):
        self.set_edit_text(value)

    def _xmlui_get_value(self):
        return self.get_edit_text()


class LiberviaTUITextBoxWidget(
    xmlui.TextBoxWidget, sat_widgets.AdvancedEdit, LiberviaTUIEvents
):
    def __init__(self, _xmlui_parent, value, read_only=False):
        sat_widgets.AdvancedEdit.__init__(self, edit_text=value, multiline=True)
        self.read_only = read_only

    def selectable(self):
        if self.read_only:
            return False
        return super(LiberviaTUITextBoxWidget, self).selectable()

    def _xmlui_set_value(self, value):
        self.set_edit_text(value)

    def _xmlui_get_value(self):
        return self.get_edit_text()


class LiberviaTUIBoolWidget(xmlui.BoolWidget, urwid.CheckBox, LiberviaTUIEvents):
    def __init__(self, _xmlui_parent, state, read_only=False):
        urwid.CheckBox.__init__(self, "", state=state)
        self.read_only = read_only

    def selectable(self):
        if self.read_only:
            return False
        return super(LiberviaTUIBoolWidget, self).selectable()

    def _xmlui_set_value(self, value):
        self.set_state(value == "true")

    def _xmlui_get_value(self):
        return C.BOOL_TRUE if self.get_state() else C.BOOL_FALSE


class LiberviaTUIIntWidget(xmlui.IntWidget, sat_widgets.AdvancedEdit, LiberviaTUIEvents):
    def __init__(self, _xmlui_parent, value, read_only=False):
        sat_widgets.AdvancedEdit.__init__(self, edit_text=value)
        self.read_only = read_only

    def selectable(self):
        if self.read_only:
            return False
        return super(LiberviaTUIIntWidget, self).selectable()

    def _xmlui_set_value(self, value):
        self.set_edit_text(value)

    def _xmlui_get_value(self):
        return self.get_edit_text()


class LiberviaTUIButtonWidget(
    xmlui.ButtonWidget, sat_widgets.CustomButton, LiberviaTUIEvents
):
    def __init__(self, _xmlui_parent, value, click_callback):
        sat_widgets.CustomButton.__init__(self, value, on_press=click_callback)

    def _xmlui_on_click(self, callback):
        urwid.connect_signal(self, "click", callback)


class LiberviaTUIListWidget(xmlui.ListWidget, sat_widgets.List, LiberviaTUIEvents):
    def __init__(self, _xmlui_parent, options, selected, flags):
        sat_widgets.List.__init__(self, options=options, style=flags)
        self._xmlui_select_values(selected)

    def _xmlui_select_value(self, value):
        return self.select_value(value)

    def _xmlui_select_values(self, values):
        return self.select_values(values)

    def _xmlui_get_selected_values(self):
        return [option.value for option in self.get_selected_values()]

    def _xmlui_add_values(self, values, select=True):
        current_values = self.get_all_values()
        new_values = copy.deepcopy(current_values)
        for value in values:
            if value not in current_values:
                new_values.append(value)
        if select:
            selected = self._xmlui_get_selected_values()
        self.change_values(new_values)
        if select:
            for value in values:
                if value not in selected:
                    selected.append(value)
            self._xmlui_select_values(selected)


class LiberviaTUIJidsListWidget(xmlui.ListWidget, sat_widgets.List, LiberviaTUIEvents):
    def __init__(self, _xmlui_parent, jids, styles):
        sat_widgets.List.__init__(
            self,
            options=jids + [""],  # the empty field is here to add new jids if needed
            option_type=lambda txt, align: sat_widgets.AdvancedEdit(
                edit_text=txt, align=align
            ),
            on_change=self._on_change,
        )
        self.delete = 0

    def _on_change(self, list_widget, jid_widget=None, text=None):
        if jid_widget is not None:
            if jid_widget != list_widget.contents[-1] and not text:
                # if a field is empty, we delete the line (except for the last line)
                list_widget.contents.remove(jid_widget)
            elif jid_widget == list_widget.contents[-1] and text:
                # we always want an empty field as last value to be able to add jids
                list_widget.contents.append(sat_widgets.AdvancedEdit())

    def _xmlui_get_selected_values(self):
        # XXX: there is not selection in this list, so we return all non empty values
        return [jid_ for jid_ in self.get_all_values() if jid_]


class LiberviaTUIAdvancedListContainer(
    xmlui.AdvancedListContainer, sat_widgets.TableContainer, LiberviaTUIEvents
):
    def __init__(self, _xmlui_parent, columns, selectable="no"):
        options = {"ADAPT": ()}
        if selectable != "no":
            options["HIGHLIGHT"] = ()
        sat_widgets.TableContainer.__init__(
            self, columns=columns, options=options, row_selectable=selectable != "no"
        )

    def _xmlui_append(self, widget):
        self.add_widget(widget)

    def _xmlui_add_row(self, idx):
        self.set_row_index(idx)

    def _xmlui_get_selected_widgets(self):
        return self.get_selected_widgets()

    def _xmlui_get_selected_index(self):
        return self.get_selected_index()

    def _xmlui_on_select(self, callback):
        """Call callback with widget as only argument"""
        urwid.connect_signal(self, "click", self._event_callback, callback)


class LiberviaTUIPairsContainer(xmlui.PairsContainer, sat_widgets.TableContainer):
    def __init__(self, _xmlui_parent):
        options = {"ADAPT": (0,), "HIGHLIGHT": (0,)}
        if self._xmlui_main.type == "param":
            options["FOCUS_ATTR"] = "param_selected"
        sat_widgets.TableContainer.__init__(self, columns=2, options=options)

    def _xmlui_append(self, widget):
        if isinstance(widget, LiberviaTUIEmptyWidget):
            # we don't want highlight on empty widgets
            widget = urwid.AttrMap(widget, "default")
        self.add_widget(widget)


class LiberviaTUILabelContainer(LiberviaTUIPairsContainer, xmlui.LabelContainer):
    pass


class LiberviaTUITabsContainer(xmlui.TabsContainer, sat_widgets.TabsContainer):
    def __init__(self, _xmlui_parent):
        sat_widgets.TabsContainer.__init__(self)

    def _xmlui_append(self, widget):
        self.body.append(widget)

    def _xmlui_add_tab(self, label, selected):
        tab = LiberviaTUIVerticalContainer(None)
        self.add_tab(label, tab, selected)
        return tab


class LiberviaTUIVerticalContainer(xmlui.VerticalContainer, urwid.ListBox):
    BOX_HEIGHT = 5

    def __init__(self, _xmlui_parent):
        urwid.ListBox.__init__(self, urwid.SimpleListWalker([]))
        self._last_size = None

    def _xmlui_append(self, widget):
        if "flow" not in widget.sizing():
            widget = urwid.BoxAdapter(widget, self.BOX_HEIGHT)
        self.body.append(widget)

    def render(self, size, focus=False):
        if size != self._last_size:
            (maxcol, maxrow) = size
            if self.body:
                widget = self.body[0]
                if isinstance(widget, urwid.BoxAdapter):
                    widget.height = maxrow
            self._last_size = size
        return super(LiberviaTUIVerticalContainer, self).render(size, focus)


### Dialogs ###


class LiberviaTUIDialog(object):
    def __init__(self, _xmlui_parent):
        self.host = _xmlui_parent.host

    def _xmlui_show(self):
        self.host.show_pop_up(self)

    def _xmlui_close(self):
        self.host.remove_pop_up(self)


class LiberviaTUIMessageDialog(LiberviaTUIDialog, xmlui.MessageDialog, sat_widgets.Alert):
    def __init__(self, _xmlui_parent, title, message, level):
        LiberviaTUIDialog.__init__(self, _xmlui_parent)
        xmlui.MessageDialog.__init__(self, _xmlui_parent)
        sat_widgets.Alert.__init__(
            self, title, message, ok_cb=lambda __: self._xmlui_close()
        )


class LiberviaTUINoteDialog(xmlui.NoteDialog, LiberviaTUIMessageDialog):
    # TODO: separate NoteDialog
    pass


class LiberviaTUIConfirmDialog(
    LiberviaTUIDialog, xmlui.ConfirmDialog, sat_widgets.ConfirmDialog
):
    def __init__(self, _xmlui_parent, title, message, level, buttons_set):
        LiberviaTUIDialog.__init__(self, _xmlui_parent)
        xmlui.ConfirmDialog.__init__(self, _xmlui_parent)
        sat_widgets.ConfirmDialog.__init__(
            self,
            title,
            message,
            no_cb=lambda __: self._xmlui_cancelled(),
            yes_cb=lambda __: self._xmlui_validated(),
        )


class LiberviaTUIFileDialog(
    LiberviaTUIDialog, xmlui.FileDialog, files_management.FileDialog
):
    def __init__(self, _xmlui_parent, title, message, level, filetype):
        # TODO: message is not managed yet
        LiberviaTUIDialog.__init__(self, _xmlui_parent)
        xmlui.FileDialog.__init__(self, _xmlui_parent)
        style = []
        if filetype == C.XMLUI_DATA_FILETYPE_DIR:
            style.append("dir")
        files_management.FileDialog.__init__(
            self,
            ok_cb=lambda path: self._xmlui_validated({"path": path}),
            cancel_cb=lambda __: self._xmlui_cancelled(),
            message=message,
            title=title,
            style=style,
        )


class GenericFactory(object):
    def __getattr__(self, attr):
        if attr.startswith("create"):
            cls = globals()[
                "LiberviaTUI" + attr[6:]
            ]  # XXX: we prefix with "LiberviaTUI" to work around an Urwid bug, WidgetMeta in Urwid don't manage multiple inheritance with same names
            return cls


class WidgetFactory(GenericFactory):
    def __getattr__(self, attr):
        if attr.startswith("create"):
            cls = GenericFactory.__getattr__(self, attr)
            cls._xmlui_main = self._xmlui_main
            return cls


class XMLUIPanel(xmlui.XMLUIPanel, LiberviaTUIWidget):
    widget_factory = WidgetFactory()

    def __init__(
        self,
        host,
        parsed_xml,
        title=None,
        flags=None,
        callback=None,
        ignore=None,
        whitelist=None,
        profile=C.PROF_KEY_NONE,
    ):
        self.widget_factory._xmlui_main = self
        self._dest = None
        xmlui.XMLUIPanel.__init__(
            self,
            host,
            parsed_xml,
            title=title,
            flags=flags,
            callback=callback,
            ignore=ignore,
            profile=profile,
        )
        LiberviaTUIWidget.__init__(self, self.main_cont, self.xmlui_title)

    def _parse_childs(
        self, _xmlui_parent, current_node, wanted=("container",), data=None
    ):
        # Small hack to always have a VerticalContainer as main container in LiberviaTUI.
        # this used to be the default behaviour for all frontends, but now
        # TabsContainer can also be the main container.
        if _xmlui_parent is self:
            node = current_node.childNodes[0]
            if node.nodeName == "container" and node.getAttribute("type") == "tabs":
                _xmlui_parent = self.widget_factory.createVerticalContainer(self)
                self.main_cont = _xmlui_parent
        return super(XMLUIPanel, self)._parse_childs(
            _xmlui_parent, current_node, wanted, data
        )

    def construct_ui(self, parsed_dom):
        def post_treat():
            assert self.main_cont.body

            if self.type in ("form", "popup"):
                buttons = []
                if self.type == "form":
                    buttons.append(urwid.Button(_("Submit"), self.on_form_submitted))
                    if not "NO_CANCEL" in self.flags:
                        buttons.append(urwid.Button(_("Cancel"), self.on_form_cancelled))
                else:
                    buttons.append(
                        urwid.Button(_("OK"), on_press=lambda __: self._xmlui_close())
                    )
                max_len = max([len(button.get_label()) for button in buttons])
                grid_wid = urwid.GridFlow(buttons, max_len + 4, 1, 0, "center")
                self.main_cont.body.append(grid_wid)
            elif self.type == "param":
                tabs_cont = self.main_cont.body[0].base_widget
                assert isinstance(tabs_cont, sat_widgets.TabsContainer)
                buttons = []
                buttons.append(sat_widgets.CustomButton(_("Save"), self.on_save_params))
                buttons.append(
                    sat_widgets.CustomButton(
                        _("Cancel"), lambda x: self.host.remove_window()
                    )
                )
                max_len = max([button.get_size() for button in buttons])
                grid_wid = urwid.GridFlow(buttons, max_len, 1, 0, "center")
                tabs_cont.add_footer(grid_wid)

        xmlui.XMLUIPanel.construct_ui(self, parsed_dom, post_treat)
        urwid.WidgetWrap.__init__(self, self.main_cont)

    def show(self, show_type=None, valign="middle"):
        """Show the constructed UI
        @param show_type: how to show the UI:
            - None (follow XMLUI's recommendation)
            - 'popup'
            - 'window'
        @param valign: vertical alignment when show_type is 'popup'.
            Ignored when show_type is 'window'.

        """
        if show_type is None:
            if self.type in ("window", "param"):
                show_type = "window"
            elif self.type in ("popup", "form"):
                show_type = "popup"

        if show_type not in ("popup", "window"):
            raise ValueError("Invalid show_type [%s]" % show_type)

        self._dest = show_type
        if show_type == "popup":
            self.host.show_pop_up(self, valign=valign)
        elif show_type == "window":
            self.host.new_widget(self, user_action=self.user_action)
        else:
            assert False
        self.host.redraw()

    def _xmlui_close(self):
        if self._dest == "window":
            self.host.remove_window()
        elif self._dest == "popup":
            self.host.remove_pop_up(self)
        else:
            raise exceptions.InternalError(
                "self._dest unknown, are you sure you have called XMLUI.show ?"
            )


class XMLUIDialog(xmlui.XMLUIDialog):
    dialog_factory = GenericFactory()


xmlui.register_class(xmlui.CLASS_PANEL, XMLUIPanel)
xmlui.register_class(xmlui.CLASS_DIALOG, XMLUIDialog)
create = xmlui.create
