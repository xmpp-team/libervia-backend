FROM debian:bookworm-slim

LABEL maintainer="Goffi <tmp_dockerfiles@goffi.org>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends locales python3-dev python3-venv python3-wheel mercurial libpq-dev gcc gnupg && \
    apt-get install -y --no-install-recommends postgresql-client && \
    # it's better to have a dedicated user
    useradd -m libervia && \
    mkdir /src && chown libervia:libervia /src && \
    # we need UTF-8 locale
    sed -i "s/# en_US.UTF-8/en_US.UTF-8/" /etc/locale.gen && locale-gen

ENV LC_ALL en_US.UTF-8

WORKDIR /home/libervia
COPY entrypoint.sh /home/libervia
RUN chown libervia:libervia /home/libervia/entrypoint.sh && chmod 0555 /home/libervia/entrypoint.sh

USER libervia
RUN python3 -m venv libervia_env && libervia_env/bin/pip install -U pip wheel && cd /src && \
    # we install thoses packages in editable mode, so we can replace them easily with volumes
    hg clone https://repos.goffi.org/sat_tmp && ~/libervia_env/bin/pip install -e sat_tmp && \
    hg clone https://repos.goffi.org/libervia-pubsub && ~/libervia_env/bin/pip install -e libervia-pubsub

ENTRYPOINT ["/home/libervia/entrypoint.sh"]
