.. _libervia-cli_notification:

====================================
notification: notifications handling
====================================

Notifications can be retrieved, expired or created with the ``notification`` commands.

add
====

Add and broadcast a new notification. Only admin profiles can add a notification.

It can be used for service announcement, such as planned maintenance.

To make an announcement to all profiles, use the ``-g, --is-global``. Note that such
global notification aren't deleted when using :ref:`libervia-cli_notification_expire`
command: you have either to use ``-e EXPIRE_AT, --expire-at EXPIRE_AT`` to
set an expiration date, or to delete them manually with
:ref:`libervia-cli_notification_delete`.

example
-------

Add a global service announcement to indicate that the server will be shut down. The
expiration date is set so that the notification will be expired after expected end of
maintenance::

  $ li notification add service "Server will be shut down for maintenance." -P HIGH -g -e "2023-10-22"

get
===

Retrieve notification for current profile. If ``-f, --follow`` argument is used, the CLI
will continue to run and show any new incoming notification.

examples
--------

Louise wishes to obtain her current notifications and display any new notifications as
they are issued::

  $ li notification get -f

Louise wants to see all her chat notifications which are at least of high priority (this
includes urgent ones)::

  $ li notification get -t chat -P HIGH

.. _libervia-cli_notification_delete:

delete
======

Delete a notification by its ID. The notification must belong to the profile doing the
request.

If ``-g, --is-global`` is used, a global notification is to be deleted. Only admins can
delete a global notification.

example
-------

Louise (who is an admin) wants to delete a previous global service announcement with ID ``123``::

  $ li notification delete -g 123

.. _libervia-cli_notification_expire:

expire
=======

Delete expired notification (the ones with an expiration date set).

If ``-l TIME_PATTERN, --limit TIME_PATTERN`` is used, also delete notification which have
been **created** before the given limit. Some notifications can't be deleted this way: if
they are global, or if they require a user action. Using ``now`` as time pattern will
clear all the notifications that can be deleted.

By default, only notifications of requesting profile are deleted (except for expired ones
which are always deleted). If ``-a, --all`` is used, notification for all profiles can be
deleted. This option is notably useful to schedule automatic notification cleaning.

example
-------

Louise want to expire notification for all profiles that are older than 7 days::

  $ li notification expire -a -l "7 days ago"
