.. _libervia-cli_blog_attachments:

===================================================
blog/attachments: (un)attach Metadata to Blog Items
===================================================

Blog attachments subcommands are the same as  :ref:`pubsub attachments
<libervia-cli_pubsub_attachments>` subcommands except that if ``--node`` is not specified,
default blog node will be used. Please refer to pubsub attachments documentation for
details.
