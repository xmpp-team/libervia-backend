#!/usr/bin/env python3

# Copyright (C) 2009-2022 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Dict, Any

from twisted.words.xish import domish

from libervia.backend.core.constants import Const as C
from libervia.backend.core.i18n import _
from libervia.backend.core.log import getLogger
from libervia.backend.core import exceptions

log = getLogger(__name__)


PLUGIN_INFO = {
    C.PI_NAME: "URL Address Information",
    C.PI_IMPORT_NAME: "XEP-0103",
    C.PI_TYPE: "XEP",
    C.PI_MODES: C.PLUG_MODE_BOTH,
    C.PI_PROTOCOLS: ["XEP-0103"],
    C.PI_MAIN: "XEP_0103",
    C.PI_HANDLER: "no",
    C.PI_DESCRIPTION: _("""Implementation of XEP-0103 (URL Address Information)"""),
}

NS_URL_DATA = "http://jabber.org/protocol/url-data"


class XEP_0103:
    namespace = NS_URL_DATA

    def __init__(self, host):
        log.info(_("XEP-0103 (URL Address Information) plugin initialization"))
        host.register_namespace("url-data", NS_URL_DATA)

    def get_url_data_elt(self, url: str, **kwargs) -> domish.Element:
        """Generate the element describing the URL

        @param url: URL to use
        @param extra: extra metadata describing how to access the URL
        @return: ``<url-data/>`` element
        """
        url_data_elt = domish.Element((NS_URL_DATA, "url-data"))
        url_data_elt["target"] = url
        return url_data_elt

    def parse_url_data_elt(self, url_data_elt: domish.Element) -> Dict[str, Any]:
        """Parse <url-data/> element

        @param url_data_elt: <url-data/> element
            a parent element can also be used
        @return: url-data data. It's a dict whose keys correspond to
            [get_url_data_elt] parameters
        @raise exceptions.NotFound: no <url-data/> element has been found
        """
        if url_data_elt.name != "url-data":
            try:
                url_data_elt = next(url_data_elt.elements(NS_URL_DATA, "url-data"))
            except StopIteration:
                raise exceptions.NotFound
        try:
            data: Dict[str, Any] = {"url": url_data_elt["target"]}
        except KeyError:
            raise ValueError(f'"target" attribute is missing: {url_data_elt.toXml}')

        return data
