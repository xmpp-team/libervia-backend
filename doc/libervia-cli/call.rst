.. _libervia-cli_call:

===============
call: A/V Calls
===============

``call`` lets you initiate or receive calls from the terminal.


Common UI
=========

There are various UI available to display a call. By default, one will be automatically
selected according to platform. To select an UI, use the ``--output, -O`` option, with one
of the following value:

default
-------

The default value make a guess on the best output to use on the current platform.

.. note::

   For the moment, ``default`` tries ``gui`` first, and fallbacks to ``simple`` if it's
   not possible.

simple
------

During a call, a simple graphical window will be created, displaying your correspondent's
video, with your own video feed in the bottom right corner.

With the UI present (by default), there are 3 buttons:

- one to (un)mute video with the ``v`` key shortcut
- one to (un)mute audio with the ``m`` key shortcut
- one to hang up with the ``h`` key shortcut. ``Ctrl+c`` will also have the same effect.

The following output options can be used:

- **split**: use 2 windows, one for local feedback and the other for remote video, instead of
  the default single one with local feedback use with Picture-in-Picture.

- **size=widthxheight**: specify the size of the destination windows (when **split** is
  not used). By default, 1280x720 is used.

tui
---

A Text User Interface can be used; it will then output the video streams directly in the
terminal.

To make it work, you need to have a terminal compatible with at least one image
standard (Kitty, iTerm2, or "block", which uses Unicode half blocks and 24-bit color
escape codes).

The control buttons are the same as for the ``simple`` output.

.. note::

   `term-image`_ is used to display images in the terminal. Check its documentation to see
   if your terminal is compatible with it (or just try). You can check compatibility at
   https://term-image.readthedocs.io/en/stable/start/installation.html#supported-terminal-emulators

.. note::

   You need to have installed the ``term-image`` package to make it work. It is the case
   if you have used `[TUI]` or `[all]` dependencies during Libervia installation.

.. _term-image: https://term-image.readthedocs.io/en/stable/


The following output options can be used:

- **renderer=RENDERER**: Use a specific renderer. It is case-insensitive and RENDERER can
  be one of:

  - **auto** (default): selects the best possible renderer automatically
  - **block**: uses Unicode half blocks. This option may be less resource-intensive than
    other options (but not necessarily, depending on the implementation).
  - **iterm2**: iTerm2 image protocol
- **fps**: frames per second to render. It defaults to 25, you can try lower values if
  your machine struggles to show images.
- **size**: same as for ``simple`` output, except the default here is 640x380 for
  performance reasons.

gui
---

The ``gui`` option enables a full graphical user interface for handling calls, akin to
other frontends (web, desktop). This GUI provides a comprehensive and interactive
experience for call management directly from your CLI environment, ensuring quick access
to the call feature from the CLI frontend.

This interface is accessible if your system supports X11 or Wayland and requires ``PyQt``
v6 or higher. Here's how to navigate and use the GUI:

.. note::

   You need to have installed the ``PyQt6`` package to make it work. It is the case if you
   have used `[GUI]` or `[all]` dependencies during Libervia installation.

Call Interface
^^^^^^^^^^^^^^

The call interface is made with:

- **Video Feeds**: The main window displays the other party's video, with your video in
  the bottom right.
- **Fullscreen Mode**: A fullscreen toggle button is located at the top right of the
  window.
- **Controls**: The following buttons are available at the bottom of your screen:

  - Mute/Unmute video or audio with respective buttons.
  - Desktop Sharing.
  - Hang up.

Desktop Sharing
^^^^^^^^^^^^^^^
To share your desktop, you need to be in a supported environment (X11 or Wayland).

For Wayland, you'll need to have the ``xdg-desktop-portal`` package installed, along with
its sibling desktop environment-specific package (e.g., ``xdg-desktop-portal-gtk`` or
``xdg-desktop-portal-kde``).

Once you've clicked on the desktop sharing button, you'll have to select what you want to
share. It can be your entire screen or a specific application window. On X11, a simple
dialog will appear, while for Wayland, a dialog provided by your environment will ask you
to select what you want to share.

example
-------

Pierre wants to call Louise with a GUI::

  $ li call make louise@example.org -O gui

Louise is expecting Pierre call, and will use TUI with the ``block`` renderer to show it::

  $ li call receive -a pierre@example.net -O tui --oo renderer=block

make
====

Make a call. The primary argument is the JID of the entity you wish to call.

example
-------

Pierre wants to call Louise::

  $ li call make louise@example.org

receive
=======

Receive a call. By default, you'll see a confirmation prompt when someone is calling; you
can then use ``y`` to accept the call or ``n`` to reject it. Use the ``-a JID,
--auto-accept JID`` option to automatically accept calls from a specific entity (can be
used multiple times), or the ``--auto-accept-all`` to accept any incoming call.

.. note::

  Accepting a call automatically activates your webcam and microphone, and shares your IP
  address with the caller. Therefore, using ``--auto-accept-all`` is a security risk. Only
  use it if you have a very good reason to do so.


examples
--------

Louise is expecting a call. When she receives one, a prompt will ask her to confirm and
start it::

  $ li call receive

Piotr has a device with a webcam and microphone for observing wildlife. He set the device
to automatically accept his calls::

  $ li call receive -a piotr@example.net

.. note::

  Libervia CLI will exit once the first accepted call is terminated. Looping in a shell
  may be necessary to call the same device multiple times.

.. note::

   Since using auto-accept mode activates the webcam and microphone, consider the privacy
   implications and ensure that no one will be filmed or recorded without their consent.

