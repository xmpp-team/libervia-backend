.. _libervia-cli_pubsub_signature:

=================================================
pubsub/signature: Sign and Check Items Signatures
=================================================

``signature`` group commands to handle pubsub item cryptographic signatures:

You can check :ref:`pubsub-encryption` to have overview on how it works.

sign
=====

Sign a pubsub item using OpenPGP keys of the profile.

example
-------

Louise want to sign an item on her blog::

  $ li pubsub signature sign -s louise@example.net -n urn:xmpp:microblog:0 -i about_feminism

check
=====

Check validity of a signature found in attachments. For now, the attachments data JSON as
found in attachments must be used, it is planned to make this command simpler in the close
future.

example
-------

Pierre wants to check that a blog post is really from Louise::

  $ li pubsub signature check -s louise@example.net -n urn:xmpp:microblog:0 -i about_feminism '{"timestamp": 1667079677.0, "signers": ["louise@example.net"], "signature": "iQGzBAABCAAdFiEEeTqK7iUsJz97sLSEgjG1Of9Gvi0FAmNdnf0ACgkQgjG1Of9Gvi293Qv+IWe6gkaAPzGI6fis9yg9biSdFE9qf9oi+jyTiZuTR1QjjZYE7x+vsj3KewKBUMOyqxKMk1WVKmimDb3yCqfF852lEox3OQuBMP46G9UHDNII6D+oqe4VVWZhqe3s3twWYc08nrvAUH4tMI96xml4wuEk6Fv8PwBcNEEzX8zNwLYeY6tsR72XErBDn5oY06OGhZeLBn6FkUI1QZ3jT0djjH87CuH+9gOS6vEIpO+UnCqvB3aqxwhHIIOh5X9bUyEyxINb3kA6WpYumlc92vE8ROndPZGEni9OfouTjJL7+tUmcNz4RJdbEzBpETtzJ4B3r/kbJWKi5YDpZZOI1k/W/eKXFIg6Aycc0iG/THSEwvYMFGrmyQYosdC+mbjSE4DvYE1aSHOLW3ut5kNuDqLH17/kyOh+VoR4FAkRtGoa34sQN2jaRG8m/Gl+98Ha67k8OisnrHOx7FKGGcrpEp+BCUgvNP0IRFCz26xSaZ6aXK2SQkcvlbVXf6o0BkE5Hwcn"}'
