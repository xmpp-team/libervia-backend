===========================
bookmarks: Manage bookmarks
===========================
Bookmarks are used to store a list of group chat rooms and indicate which ones are
currently joined. Libervia offers commands to list, set, and remove bookmarks.

list
====

Get and print available bookmarks.

example
-------
Retrieve all bookmarks::

  $ li bookmarks list

set
===

Add or update a bookmark.

You can use ``-n NAME, --name NAME`` to specify a name for the bookmark. This option
allows you to provide a human-readable name for the bookmark, making it easier to
identify.

``-N NICK, --nick NICK`` set the nickname to use in the room.

``-j [BOOL], --join [BOOL]`` indicates whether you want to join the room. If the value is not set or ``true``, the room will be joined; otherwise, it will be left if the user is currently in it.

example
-------
Add a bookmark to a chat room with a name and join it::

  $ li bookmarks set lunch@conference.example.net -n "Lunch Organization" -j

remove
======
Remove a bookmark.

example
-------
Remove a bookmark for a chat room::

  $ li bookmarks remove lunch@conference.example.net

legacy
======

Manage legacy bookmarks. Please check :ref:`libervia-cli_bookmarks_legacy`.
