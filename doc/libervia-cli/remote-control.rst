.. _libervia-cli_remote-control:

==============================
remote-control: Remote Control
==============================

The ``remote-control`` feature allows you to control another device or be controlled by another device.

send
====

Send control events to another device. For now, it only sends keyboard events.

example
-------

Louise wants to control Pierre's device::

  $ li remote-control send pierre@example.net


receive
=======

Use ``receive`` to establish a connection with another device and accept incoming control
events. This feature uses the freedesktop.org's Remote Desktop portal, which must be
installed on your system and compatible with your platform. On most Wayland-based desktop
environments, this portal is usually available.

Immediately upon connecting, Libervia CLI requests permission to initiate the remote
control session, event if not request has been received yet. This ensures that the
connection can be established without requiring manual intervention to authorize access.

After a connection has been accepted and terminated, the command quits.

``--verbose, -v`` can be used to show received input events.
If you don't expect to share screen, use ``S {yes,no,auto}, --share-screen {yes,no,auto}``
with a value of ``no``.

example
-------

Louise is expecting Piotr to control her device and wants to automatically accept control from him::

  $ li remote-control receive louise@example.org
