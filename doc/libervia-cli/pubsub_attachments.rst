.. _libervia-cli_pubsub_attachments:

=======================================================
pubsub/attachments: (Un)Attach Metadata to Pubsub Items
=======================================================

``attachments`` is a subcommand grouping all pubsub commands related to "pubsub
attachments" specification.

With them, you can (un)attach data to any pubsub item. This is notably used to handle
data like saying if an item is (un)noticed, or add emoji reactions.

get
===

Retrieve a list of all attached data to an item.

By default all attachments of the item are returned, this can be filtered by using ``-j
JIDS, --jid JIDS``

example
-------

Louise check all attachments on her last blog post::

  $ li pubsub attachments get -s louise@example.org -n urn:xmpp:microblog:0 -i some-news-acf0

set
===

Update or replace attachments. By default new attachments are updated, but if the ``-R,
--replace`` is used, new attachments replace the whole former one where it make sense. For
instance, ``-R`` doesn't change anything for ``noticed`` attachments, but it will replace
all reactions (potentially with no reaction at all), where the default behaviour is to
merge former and new reactions.

Note that only specified attachments are affected, if unknown or unspecified attachments
exist, they will stay unmodified.

For now, only ``noticed`` (to say that an element has been seen and taken into account)
and ``reactions`` (emojis to show emotion or other kind of reaction about something) are
managed.

``-N [BOOLEAN], --noticed [BOOLEAN]`` takes on optional argument to say if the item is
noticed or not. If the optional argument is not specified, if will be the same as if the
``true`` value was used.

examples
--------

Pierre wants to indicate to Louise that is has seen and he took into account her last blog post::

  $ li pubsub attachments set -s louise@example.org -n urn:xmpp:microblog:0 -i some-news-acf0 -N

Louise wants to react to Pierre blog post about night trains::

  $ li pubsub attachments set -s pierre@example.net -n urn:xmpp:microblog:0 -i nigh-train-are-great-f120 -r 🚆🌜💤
