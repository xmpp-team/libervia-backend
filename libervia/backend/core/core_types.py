#!/usr/bin/env python3

# Libervia types
# Copyright (C) 2011  Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import namedtuple
from typing import Dict, Callable, Optional
from typing_extensions import TypedDict

from twisted.words.protocols.jabber import jid as t_jid
from twisted.words.protocols.jabber import xmlstream
from twisted.words.xish import domish
from wokkel import disco


class SatXMPPEntity:

    profile: str
    jid: t_jid.JID
    is_admin: bool
    is_component: bool
    server_jid: t_jid.JID
    IQ: Callable[[Optional[str], Optional[int]], xmlstream.IQ]
    identities: list[disco.DiscoIdentity]


EncryptionPlugin = namedtuple(
    "EncryptionPlugin", ("instance", "name", "namespace", "priority", "directed")
)


class EncryptionSession(TypedDict):
    plugin: EncryptionPlugin


# Incomplete types built through observation rather than code inspection.
MessageDataExtra = TypedDict(
    "MessageDataExtra", {"encrypted": bool, "origin_id": str}, total=False
)


MessageData = TypedDict(
    "MessageData",
    {
        "from": t_jid.JID,
        "to": t_jid.JID,
        "uid": str,
        "message": Dict[str, str],
        "subject": Dict[str, str],
        "type": str,
        "timestamp": float,
        "extra": MessageDataExtra,
        "ENCRYPTION": EncryptionSession,
        "xml": domish.Element,
    },
    total=False,
)
