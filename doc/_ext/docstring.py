#!/usr/bin/env python3

"""Adapt Libervia docstring style to autodoc"""


def process_docstring(app, what, name, obj, options, lines):
    lines[:] = [
        l.replace("@param", ":param").replace("@raise", ":raises")
        for l in lines
    ]


def setup(app):
    app.connect("autodoc-process-docstring", process_docstring)
    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
