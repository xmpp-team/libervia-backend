#!/usr/bin/env python3

# Libervia plugin for Jingle (XEP-0166)
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import abc
from pathlib import Path

from libervia.backend.core.i18n import _


class AppManagerBackend(abc.ABC):
    """Abstract class for App Manager."""

    name: str
    discover_path: Path | None = None

    def __init__(self, host) -> None:
        """Initialize the App Manager.

        @param host: The host object.
        """
        self.host = host
        self._am = host.plugins["APP_MANAGER"]
        self._am.register(self)

    @abc.abstractmethod
    async def start(self, app_data: dict) -> None:
        """Start the app.

        @param app_data: A dictionary containing app data.
        """
        pass

    @abc.abstractmethod
    async def stop(self, app_data: dict) -> None:
        """Stop the app.

        @param app_data: A dictionary containing app data.
        """
        pass

    @abc.abstractmethod
    async def compute_expose(self, app_data: dict) -> None:
        """Compute exposed data for the app.

        @param app_data: A dictionary containing app data.
        """
        pass
