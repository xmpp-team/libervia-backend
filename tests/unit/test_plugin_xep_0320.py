#!/usr/bin/env python3

# Libervia: an XMPP client
# Copyright (C) 2009-2023 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.words.xish import domish

from libervia.backend.plugins.plugin_xep_0320 import NS_JINGLE_DTLS, XEP_0320
from libervia.backend.tools import xml_tools


class TestXEP0320:
    def test_parse_transport_trigger(self, host):
        """<transport> element is parsed correctly in trigger"""
        xep_0320 = XEP_0320(host)

        transport_elt = xml_tools.parse(
            f"<transport><fingerprint xmlns='{NS_JINGLE_DTLS}' hash='sha-256' "
            "setup='active'>6D:8F:6A:53:A3:7E:10:B2:58:16:AB:A3:92:6F:8A:5B:2D:55:1C:FB:"
            "2F:E3:6E:94:FE:4F:4E:FE:D4:77:49:B6</fingerprint></transport>"
        )

        ice_data = {}
        result = xep_0320._parse_transport_trigger(transport_elt, ice_data)

        expected_ice_data = {
            "fingerprint": {
                "hash": "sha-256",
                "setup": "active",
                "fingerprint": "6D:8F:6A:53:A3:7E:10:B2:58:16:AB:A3:92:6F:8A:5B:2D:55:1C:"
                "FB:2F:E3:6E:94:FE:4F:4E:FE:D4:77:49:B6",
            },
        }

        assert ice_data == expected_ice_data
        assert result

    def test_build_transport(self, host):
        """<transport> element is buid correctly in trigger"""
        xep_0320 = XEP_0320(host)

        transport_elt = domish.Element((None, "transport"))

        ice_data = {
            "fingerprint": {
                "hash": "sha-256",
                "setup": "active",
                "fingerprint": "6D:8F:6A:53:A3:7E:10:B2:58:16:AB:A3:92:6F:8A:5B:2D:55:1C:"
                "FB:2F:E3:6E:94:FE:4F:4E:FE:D4:77:49:B6",
            },
        }

        result = xep_0320._build_transport_trigger(transport_elt, ice_data)

        expected_transport_elt = xml_tools.parse(
            f"<transport><fingerprint xmlns='{NS_JINGLE_DTLS}' hash='sha-256' "
            "setup='active'>6D:8F:6A:53:A3:7E:10:B2:58:16:AB:A3:92:6F:8A:5B:2D:55:1C:FB:"
            "2F:E3:6E:94:FE:4F:4E:FE:D4:77:49:B6</fingerprint></transport>"
            ""
        )

        assert transport_elt.toXml() == expected_transport_elt.toXml()
        assert result
