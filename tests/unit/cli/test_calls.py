#!/usr/bin/env python3

# Libervia: an XMPP client
# Copyright (C) 2009-2024 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from unittest.mock import MagicMock, patch

import pytest

try:
    from libervia.cli.call_gui import AVCallUI
    from libervia.frontends.tools import display_servers
    from PyQt6.QtWidgets import QApplication
except ModuleNotFoundError:
    pytest.skip("PyQt6 is not available.", allow_module_level=True)


@pytest.fixture(scope="session")
def qapplication():
    # QApplication is needed to instantiate any QWidget
    return QApplication([])


@pytest.fixture
def av_call_gui(qapplication):
    host = MagicMock()
    icons_path = MagicMock()
    gui = AVCallUI(host, icons_path)
    gui.webrtc_call = MagicMock()
    return gui


def test_toggle_fullscreen(av_call_gui):
    """``toggle_fullscreen`` changes window state."""
    initial_state = av_call_gui.isFullScreen()
    av_call_gui.toggle_fullscreen()
    assert av_call_gui.isFullScreen() != initial_state


def test_toggle_video(av_call_gui):
    """``toggle_video`` actually toggles video state."""
    av_call_gui.webrtc_call.webrtc = MagicMock(video_muted=False)
    av_call_gui.toggle_video()
    assert av_call_gui.webrtc_call.webrtc.video_muted is True


def test_toggle_audio(av_call_gui):
    """``toggle_audio`` actually toggles audio state."""
    av_call_gui.webrtc_call.webrtc = MagicMock(audio_muted=False)
    av_call_gui.toggle_audio()
    assert av_call_gui.webrtc_call.webrtc.audio_muted is True


def test_share_desktop(av_call_gui):
    """``share_desktop`` changes WebRTC ``desktop_sharing`` state."""
    av_call_gui.webrtc_call.webrtc = MagicMock(desktop_sharing=False)
    # We test WAYLAND as it is a direct change of ``desktop_sharing`` state.
    with patch(
        "libervia.cli.call_gui.display_servers.detect",
        return_value=display_servers.WAYLAND,
    ):
        av_call_gui.share_desktop()
        assert av_call_gui.webrtc_call.webrtc.desktop_sharing is True


def test_hang_up(av_call_gui):
    """Hang-up button triggers window close."""
    with patch.object(av_call_gui, "close") as mock_close:
        av_call_gui.hang_up()
    mock_close.assert_called_once()
