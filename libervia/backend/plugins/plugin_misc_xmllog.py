#!/usr/bin/env python3


# SàT plugin for managing raw XML log
# Copyright (C) 2011  Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from libervia.backend.core.i18n import _
from libervia.backend.core.constants import Const as C
from libervia.backend.core.log import getLogger
from twisted.words.xish import domish
from functools import partial

log = getLogger(__name__)

PLUGIN_INFO = {
    C.PI_NAME: "Raw XML log Plugin",
    C.PI_IMPORT_NAME: "XmlLog",
    C.PI_TYPE: "Misc",
    C.PI_PROTOCOLS: [],
    C.PI_DEPENDENCIES: [],
    C.PI_MAIN: "XmlLog",
    C.PI_HANDLER: "no",
    C.PI_DESCRIPTION: _("""Send raw XML logs to bridge"""),
}


class XmlLog(object):

    params = """
    <params>
    <general>
    <category name="Debug">
        <param name="Xml log" label="%(label_xmllog)s" value="false" type="bool" />
    </category>
    </general>
    </params>
    """ % {
        "label_xmllog": _("Activate XML log")
    }

    def __init__(self, host):
        log.info(_("Plugin XML Log initialization"))
        self.host = host
        host.memory.update_params(self.params)
        host.bridge.add_signal(
            "xml_log", ".plugin", signature="sss"
        )  # args: direction("IN" or "OUT"), xml_data, profile

        host.trigger.add("stream_hooks", self.add_hooks)

    def add_hooks(self, client, receive_hooks, send_hooks):
        self.do_log = self.host.memory.param_get_a("Xml log", "Debug")
        if self.do_log:
            receive_hooks.append(partial(self.on_receive, client=client))
            send_hooks.append(partial(self.on_send, client=client))
            log.info(_("XML log activated"))
        return True

    def on_receive(self, element, client):
        self.host.bridge.xml_log("IN", element.toXml(), client.profile)

    def on_send(self, obj, client):
        if isinstance(obj, str):
            xml_log = obj
        elif isinstance(obj, domish.Element):
            xml_log = obj.toXml()
        else:
            log.error(_("INTERNAL ERROR: Unmanaged XML type"))
        self.host.bridge.xml_log("OUT", xml_log, client.profile)
