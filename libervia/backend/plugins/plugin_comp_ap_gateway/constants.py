#!/usr/bin/env python3

# Libervia ActivityPub Gateway
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


IMPORT_NAME = "ap-gateway"
CONF_SECTION = f"component {IMPORT_NAME}"
CONTENT_TYPE_WEBFINGER = "application/jrd+json; charset=utf-8"
CONTENT_TYPE_AP = "application/activity+json; charset=utf-8"
TYPE_ACTOR = "actor"
TYPE_INBOX = "inbox"
TYPE_SHARED_INBOX = "shared_inbox"
TYPE_OUTBOX = "outbox"
TYPE_FOLLOWERS = "followers"
TYPE_FOLLOWING = "following"
TYPE_ITEM = "item"
TYPE_TOMBSTONE = "Tombstone"
TYPE_MENTION = "Mention"
TYPE_LIKE = "Like"
TYPE_REACTION = "EmojiReact"
TYPE_EVENT = "Event"
TYPE_JOIN = "Join"
TYPE_LEAVE = "Leave"
MEDIA_TYPE_AP = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
MEDIA_TYPE_AP_ALT = "application/activity+json"
NS_AP = "https://www.w3.org/ns/activitystreams"
NS_AP_PUBLIC = f"{NS_AP}#Public"
# 3 values can be used, see https://www.w3.org/TR/activitypub/#public-addressing
PUBLIC_TUPLE = (NS_AP_PUBLIC, "as:Public", "Public")
AP_REQUEST_TYPES = {
    "GET": {TYPE_ACTOR, TYPE_OUTBOX, TYPE_FOLLOWERS, TYPE_FOLLOWING},
    "POST": {"inbox"},
}
AP_REQUEST_TYPES["HEAD"] = AP_REQUEST_TYPES["GET"]
# headers to check for signature
SIGN_HEADERS = {
    # headers needed for all HTTP methods
    None: [
        # tuples are equivalent headers/pseudo headers, one of them must be present
        ("date", "(created)"),
        ("digest", "(request-target)"),
    ],
    b"GET": ["host"],
    b"POST": ["digest"],
}
PAGE_SIZE = 10
HS2019 = "hs2019"
# delay after which a signed request is not accepted anymore
SIGN_EXP = 12 * 60 * 60  # 12 hours (same value as for Mastodon)

LRU_MAX_SIZE = 200
ACTIVITY_TYPES = (
    "Accept",
    "Add",
    "Announce",
    "Arrive",
    "Block",
    "Create",
    "Delete",
    "Dislike",
    "Flag",
    "Follow",
    "Ignore",
    "Invite",
    "Join",
    "Leave",
    "Like",
    "Listen",
    "Move",
    "Offer",
    "Question",
    "Reject",
    "Read",
    "Remove",
    "TentativeReject",
    "TentativeAccept",
    "Travel",
    "Undo",
    "Update",
    "View",
    # non-standard activities
    "EmojiReact",
)
ACTIVITY_TYPES_LOWER = [a.lower() for a in ACTIVITY_TYPES]
ACTIVITY_OBJECT_MANDATORY = (
    "Create",
    "Update",
    "Delete",
    "Follow",
    "Add",
    "Remove",
    "Like",
    "Block",
    "Undo",
)
ACTIVITY_TARGET_MANDATORY = ("Add", "Remove")
# activities which can be used with Shared Inbox (i.e. with no account specified)
# must be lowercase
ACTIVIY_NO_ACCOUNT_ALLOWED = (
    "create",
    "update",
    "delete",
    "announce",
    "undo",
    "like",
    "emojireact",
    "join",
    "leave",
)
# maximum number of parents to retrieve when comments_max_depth option is set
COMMENTS_MAX_PARENTS = 100
# maximum size of avatar, in bytes
MAX_AVATAR_SIZE = 1024 * 1024 * 5

# storage prefixes
ST_AVATAR = "[avatar]"
ST_AP_CACHE = "[AP_item_cache]"
