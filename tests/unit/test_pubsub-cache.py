#!/usr/bin/env python3

# Libervia: an XMPP client
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.internet import defer
from pytest_twisted import ensureDeferred as ed
from unittest.mock import MagicMock, patch
from libervia.backend.memory.sqla import PubsubNode, SyncState
from libervia.backend.core.constants import Const as C


class TestPubsubCache:

    @ed
    async def test_cache_is_used_transparently(self, host, client):
        """Cache is used when a pubsub get_items operation is done"""
        items_ret = defer.Deferred()
        items_ret.callback(([], {}))
        client.pubsub_client.items = MagicMock(return_value=items_ret)
        host.memory.storage.get_pubsub_node.return_value = None
        pubsub_node = host.memory.storage.set_pubsub_node.return_value = PubsubNode(
            sync_state=None
        )
        with patch.object(host.plugins["PUBSUB_CACHE"], "cache_node") as cache_node:
            await host.plugins["XEP-0060"].get_items(
                client,
                None,
                "urn:xmpp:microblog:0",
            )
            assert cache_node.call_count == 1
            assert cache_node.call_args.args[-1] == pubsub_node

    @ed
    async def test_cache_is_skipped_with_use_cache_false(self, host, client):
        """Cache is skipped when 'use_cache' extra field is False"""
        items_ret = defer.Deferred()
        items_ret.callback(([], {}))
        client.pubsub_client.items = MagicMock(return_value=items_ret)
        host.memory.storage.get_pubsub_node.return_value = None
        host.memory.storage.set_pubsub_node.return_value = PubsubNode(sync_state=None)
        with patch.object(host.plugins["PUBSUB_CACHE"], "cache_node") as cache_node:
            await host.plugins["XEP-0060"].get_items(
                client, None, "urn:xmpp:microblog:0", extra={C.KEY_USE_CACHE: False}
            )
            assert not cache_node.called

    @ed
    async def test_cache_is_not_used_when_no_cache(self, host, client):
        """Cache is skipped when 'pubsub_cache_strategy' is set to 'no_cache'"""
        with host.use_option_and_reload(None, "pubsub_cache_strategy", "no_cache"):
            items_ret = defer.Deferred()
            items_ret.callback(([], {}))
            client.pubsub_client.items = MagicMock(return_value=items_ret)
            host.memory.storage.get_pubsub_node.return_value = None
            host.memory.storage.set_pubsub_node.return_value = PubsubNode(sync_state=None)
            with patch.object(host.plugins["PUBSUB_CACHE"], "cache_node") as cache_node:
                await host.plugins["XEP-0060"].get_items(
                    client,
                    None,
                    "urn:xmpp:microblog:0",
                )
                assert not cache_node.called

    @ed
    async def test_no_pubsub_get_when_cache_completed(self, host, client):
        """No pubsub get is emitted when items are fully cached"""
        items_ret = defer.Deferred()
        items_ret.callback(([], {}))
        client.pubsub_client.items = MagicMock(return_value=items_ret)
        host.memory.storage.get_pubsub_node.return_value = PubsubNode(
            sync_state=SyncState.COMPLETED
        )
        with patch.object(
            host.plugins["PUBSUB_CACHE"], "get_items_from_cache"
        ) as get_items_from_cache:
            get_items_from_cache.return_value = ([], {})
            await host.plugins["XEP-0060"].get_items(
                client,
                None,
                "urn:xmpp:microblog:0",
            )
            assert get_items_from_cache.call_count == 1
            assert not client.pubsub_client.items.called

    @ed
    async def test_pubsub_get_when_cache_in_progress(self, host, client):
        """Pubsub get is emitted when items are currently being cached"""
        items_ret = defer.Deferred()
        items_ret.callback(([], {}))
        client.pubsub_client.items = MagicMock(return_value=items_ret)
        host.memory.storage.get_pubsub_node.return_value = PubsubNode(
            sync_state=SyncState.IN_PROGRESS
        )
        with patch.object(host.plugins["PUBSUB_CACHE"], "analyse_node") as analyse_node:
            analyse_node.return_value = {"to_sync": True}
            with patch.object(
                host.plugins["PUBSUB_CACHE"], "get_items_from_cache"
            ) as get_items_from_cache:
                get_items_from_cache.return_value = ([], {})
                assert client.pubsub_client.items.call_count == 0
                await host.plugins["XEP-0060"].get_items(
                    client,
                    None,
                    "urn:xmpp:microblog:0",
                )
                assert not get_items_from_cache.called
                assert client.pubsub_client.items.call_count == 1
