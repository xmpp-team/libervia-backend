#!/usr/bin/env python3


# Libervia CLI
# Copyright (C) 2009-2021 Jérôme Poisson (goffi@goffi.org)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
from . import base
from libervia.backend.core.i18n import _
from libervia.cli.constants import Const as C
from libervia.cli import common
from libervia.backend.tools.common.ansi import ANSI as A
from libervia.frontends.bridge.bridge_frontend import BridgeException
import codecs
import json

__commands__ = ["Forums"]

FORUMS_TMP_DIR = "forums"


class Edit(base.CommandBase, common.BaseEdit):
    use_items = False

    def __init__(self, host):
        base.CommandBase.__init__(
            self,
            host,
            "edit",
            use_pubsub=True,
            use_draft=True,
            use_verbose=True,
            help=_("edit forums"),
        )
        common.BaseEdit.__init__(self, self.host, FORUMS_TMP_DIR)

    def add_parser_options(self):
        self.parser.add_argument(
            "-k",
            "--key",
            default="",
            help=_("forum key (DEFAULT: default forums)"),
        )

    def get_tmp_suff(self):
        """return suffix used for content file"""
        return "json"

    async def publish(self, forums_raw):
        try:
            await self.host.bridge.forums_set(
                forums_raw,
                self.args.service,
                self.args.node,
                self.args.key,
                self.profile,
            )
        except Exception as e:
            self.disp(f"can't set forums: {e}", error=True)
            self.host.quit(C.EXIT_BRIDGE_ERRBACK)
        else:
            self.disp(_("forums have been edited"), 1)
            self.host.quit()

    async def start(self):
        try:
            forums_json = await self.host.bridge.forums_get(
                self.args.service,
                self.args.node,
                self.args.key,
                self.profile,
            )
        except BridgeException as e:
            if e.classname == "NotFound":
                forums_json = ""
            else:
                self.disp(f"can't get node configuration: {e}", error=True)
                self.host.quit(C.EXIT_BRIDGE_ERRBACK)

        content_file_obj, content_file_path = self.get_tmp_file()
        forums_json = forums_json.strip()
        if forums_json:
            # we loads and dumps to have pretty printed json
            forums = json.loads(forums_json)
            # cf. https://stackoverflow.com/a/18337754
            f = codecs.getwriter("utf-8")(content_file_obj)
            json.dump(forums, f, ensure_ascii=False, indent=4)
            content_file_obj.seek(0)
        await self.run_editor("forums_editor_args", content_file_path, content_file_obj)


class Get(base.CommandBase):
    def __init__(self, host):
        extra_outputs = {"default": self.default_output}
        base.CommandBase.__init__(
            self,
            host,
            "get",
            use_output=C.OUTPUT_COMPLEX,
            extra_outputs=extra_outputs,
            use_pubsub=True,
            use_verbose=True,
            help=_("get forums structure"),
        )

    def add_parser_options(self):
        self.parser.add_argument(
            "-k",
            "--key",
            default="",
            help=_("forum key (DEFAULT: default forums)"),
        )

    def default_output(self, forums, level=0):
        for forum in forums:
            keys = list(forum.keys())
            keys.sort()
            try:
                keys.remove("title")
            except ValueError:
                pass
            else:
                keys.insert(0, "title")
            try:
                keys.remove("sub-forums")
            except ValueError:
                pass
            else:
                keys.append("sub-forums")

            for key in keys:
                value = forum[key]
                if key == "sub-forums":
                    self.default_output(value, level + 1)
                else:
                    if self.host.verbosity < 1 and key != "title":
                        continue
                    head_color = C.A_LEVEL_COLORS[level % len(C.A_LEVEL_COLORS)]
                    self.disp(
                        A.color(level * 4 * " ", head_color, key, A.RESET, ": ", value)
                    )

    async def start(self):
        try:
            forums_raw = await self.host.bridge.forums_get(
                self.args.service,
                self.args.node,
                self.args.key,
                self.profile,
            )
        except Exception as e:
            self.disp(f"can't get forums: {e}", error=True)
            self.host.quit(C.EXIT_BRIDGE_ERRBACK)
        else:
            if not forums_raw:
                self.disp(_("no schema found"), 1)
                self.host.quit(1)
            forums = json.loads(forums_raw)
            await self.output(forums)


class Set(base.CommandBase):

    def __init__(self, host):
        base.CommandBase.__init__(
            self,
            host,
            "set",
            use_pubsub=True,
            help=_("set forums"),
        )

    def add_parser_options(self):
        self.parser.add_argument(
            "-k",
            "--key",
            default="",
            help=_("forum key (DEFAULT: default forums)"),
        )

    async def start(self):
        forums_raw = sys.stdin.read()
        try:
            json.loads(forums_raw)
        except Exception as e:
            self.parser.error(f"Invalid JSON, a valid JSON must be used as input: {e}")
        try:
            await self.host.bridge.forums_set(
                forums_raw,
                self.args.service,
                self.args.node,
                self.args.key,
                self.profile,
            )
        except Exception as e:
            self.disp(f"can't set forums: {e}", error=True)
            self.host.quit(C.EXIT_BRIDGE_ERRBACK)
        else:
            self.disp(_("forums have been set"))
            self.host.quit()


class Forums(base.CommandBase):
    subcommands = (Get, Set, Edit)

    def __init__(self, host):
        super(Forums, self).__init__(
            host, "forums", use_profile=False, help=_("Forums structure edition")
        )
